//
//  CoinManager.swift
//  ByteCoin
//
//  Created by Angela Yu on 11/09/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import Foundation

protocol CoinManagerDelegate {
    func didUpdatePrice(_ coinManager: CoinManager, price: Double)
    func didFailWithError(_ error: Error)
}

struct CoinManager {
    
    // MARK - Constants
    
    let baseURL = "https://rest.coinapi.io/v1/exchangerate/BTC"
    let apiKey = "61AFCC71-5342-413F-9AAD-582F1D55CB90"
    
    let currencyArray = ["AUD", "BRL","CAD","CNY","EUR","GBP","HKD","IDR","ILS","INR","JPY","MXN","NOK","NZD","PLN","RON","RUB","SEK","SGD","USD","ZAR"]

    // MARK - Variables
    
    var delegate: CoinManagerDelegate?
    
    // MARK - Functions
    
    func getCoinPrice(for currency: String) {
        let urlString = "\(baseURL)/\(currency)?apikey=\(apiKey)"
        perfomRequest(with: urlString)
    }
    
    func perfomRequest(with urlString: String) {
        guard let url = URL(string: urlString) else { return }
        
        let session = URLSession(configuration: .default)
        
        let task = session.dataTask(with: url) { (data, response, error) in
            if let error = error {
                self.delegate?.didFailWithError(error)
            }
            
            if let safeData = data {
                if let price = self.parseJSON(safeData) {
                    self.delegate?.didUpdatePrice(self, price: price)
                }
            }
        }
        
        task.resume()
    }
    
    func parseJSON(_ data: Data) -> Double? {
        let decoder = JSONDecoder()
        
        do {
            let data = try decoder.decode(CoinData.self, from: data)
            let price = data.rate
            
            return price
        } catch {
            delegate?.didFailWithError(error)
            return nil
        }
    }
}
