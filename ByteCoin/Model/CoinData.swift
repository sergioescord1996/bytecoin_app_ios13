//
//  CoinData.swift
//  ByteCoin
//
//  Created by Sergio Escalante Ordonez on 18/09/2020.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation

struct CoinData: Codable {
    let rate: Double
}
